// @File(label = "Input directory", style = "directory") input
// @File(label = "Output directory", style = "directory") output
// @String(label = "Frame Interval (s)", value = "342") interval

//
//
//
//											Written by Dave Mason [dnmason@liv.ac.uk]
//											Liverpool Centre for Cell Imaging
//											September 2017
//											
//											Licenced under CCBY 4.0
//											https://creativecommons.org/licenses/by/4.0/
//
//-------------------------------------------
// Files will have the following structure:
//
//		241014_L18_R124.lsm
//     |     | | | |  |
//		  a     b	c
//
//	a= prefix
//  b= position number
//	c= timepoint
//
//-------------------------------------------
setBatchMode(true);
suffix=".lsm"; //-- was in script parameters but doesn't need to be as never changes
dirlist = getFileList(input);
dirlist = Array.sort(dirlist);
//Array.show(dirlist); //--debug only

//-- First get the name structure by finding the first LSM file (will probably be PREFIX_L1_R1.lsm)
for (i=0;i<dirlist.length;i++){
if (endsWith(dirlist[i],suffix)){
//print("Found an lsm file: "+dirlist[i]);
tmpFile=dirlist[i];
break; //-- Requires at least 1.48d
}
//--Check here to make sure we can find a file
if (i==dirlist.length-1) {
//-- Reached the end of the list without finding something with the right suffix so quit
exit("Cannot find any files. Are the directory and/or suffix correct?");
}
}

//-- Now need to find prefix, max L and max R
ArrSplit=split(tmpFile,"_");
if (ArrSplit.length>2){
	//-- There is a prefix (which may contain underscores) so build the prefix relatively:
	prefix=ArrSplit[0];
	for (j=1;j<ArrSplit.length-3;J++){
		prefix=Array.concat(prefix,ArrSplit[j]);
		}
	
}else{
	prefix="";
}

maxL=0;
maxR=0;
for (i=0;i<dirlist.length;i++){
//-- only consider LSM files
if (endsWith(dirlist[i],suffix)){
tmp=getL(dirlist[i]);
if (tmp>maxL){maxL=tmp;}
tmp=getR(dirlist[i]);
if (tmp>maxR){maxR=tmp;}
} //-- suffix loop

} //-- filelist loop

//print("Max L: "+maxL);
//print("Max R: "+maxR);


//-- Repeat for all positions (L values)
for (l=1; l<=maxL;l++) {
//-- Open the files in order
    for (r=1; r<=maxR; r++) {
    //-- Check for exist here in case there are different numbers of files per position (there will only =< maxR)
        open(input+File.separator+prefix+"_L"+l+"_R"+r+suffix);
		//print(input+File.separator+prefix+"_L"+l+"_R"+r+suffix);
    }

//-- With all the images open, concat and figure out what the dimensionality should be
run("Concatenate...", "all_open title=Stack");
//-- Reset display
for (k=0;k<nSlices;k++){
	setSlice(k+1);
	resetMinAndMax();
}

//-- correct timecourse calibration here (in seconds)
Stack.setFrameInterval(parseFloat(interval));

//-- Save the complete stack as a tif in the output directory - zero pad position number
selectWindow("Stack");
if (prefix==""){
saveAs("Tif", output + File.separator + prefix + "L" + IJ.pad(l,lengthOf(toString(maxL))) + ".tif"); 
}else{
saveAs("Tif", output + File.separator + prefix + "_L" + IJ.pad(l,lengthOf(toString(maxL))) + ".tif"); 
}
close("*");

} //-- xyPos Loop

setBatchMode(false);


//---------------------------------------------------
// FUNCTIONS
//---------------------------------------------------
function getL(a) {
tmp_split=split(a,"_");
return  parseInt(substring(tmp_split[tmp_split.length-2],1));
}
function getR(a) {
tmp_split=split(a,"_");
return  parseInt(substring(tmp_split[tmp_split.length-1],1,lastIndexOf(tmp_split[tmp_split.length-1],".")));
}