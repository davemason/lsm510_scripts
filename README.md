# CONCATENATING LSM510 MULTIPOSITION DATASETS #
This IJ1 script is used to concatentate data from a Zeiss LSM 510 running AIM software. The Ellenberg macro for multiposition / autofocus &c, writes everthing out as single files with the following structure:

      241014_L18_R124.lsm
      |    | | | |  |
        a     b	   c
     
     a= prefix
     b= position number
     c= timepoint

Written and Tested in [Fiji](http://fiji.sc) 64bit. Requires at least ImageJ 1.48

### Usage ###

* Download and update [Fiji](http://fiji.sc)
* Open the script in the Fiji Script Editor and hit run, you'll be asked for an input folder, output directory, and also the time interval of your movie.
* Your output will be saved into the selected output folder as one tif-stack per position. 

### Acknowledgements and Licence ###

* The code was written by [Dave Mason](http://pcwww.liv.ac.uk/~dnmason), from the University of Liverpool [Centre for Cell Imaging](http://cci.liv.ac.uk)
* Provided under a [CCBY 4.0 Licence](https://creativecommons.org/licenses/by/4.0/) which allows use and modification as long as the original source is attributed.
